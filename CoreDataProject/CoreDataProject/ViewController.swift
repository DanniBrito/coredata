//
//  ViewController.swift
//  CoreDataProject
//
//  Created by Danni Brito on 23/1/18.
//  Copyright © 2018 Danni Brito. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    
    let manageObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func savePerson(){
        let entityDescription = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let person = Person(entity: entityDescription!, insertInto: manageObjectContext)
        
        person.address = addressTextField.text ?? ""
        person.name = nameTextField.text ?? ""
        person.phone = phoneTextField.text ?? ""
        
        do{
            
            try manageObjectContext.save()
            showToast(message: "Persona guardada")
            clearFields()
            
        }catch{
            showToast(message: "Error al guardar")
            print("Error al guardar")
        }
    }
    
    func clearFields(){
        addressTextField.text = ""
        phoneTextField.text = ""
        nameTextField.text = ""
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }

    @IBAction func saveButtonPressed(_ sender: Any) {
        savePerson()
    }
    
    @IBAction func findButtonPressed(_ sender: Any) {
        if nameTextField.text == "" {
            findAll()
            return
        }
        findOne()
        
    }
    
    func findAll(){
        
        
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            
            
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            for result in results{
                let person = result as! Person
                print("Name: \(person.name ?? "")", terminator: " ")
                print("Address: \(person.address ?? "")", terminator: " ")
                print("Phone: \(person.phone ?? "")", terminator: " ")
                print()   
            }
        } catch {
            showToast(message: "Error finding people")
        }
    }
    
    func findOne(){
        _ = NSEntityDescription.entity(forEntityName: "Person", in: manageObjectContext)
        
        let request: NSFetchRequest<Person> = Person.fetchRequest()
        
        let predicate = NSPredicate(format: "name = %@", nameTextField.text!)
        
        request.predicate = predicate
        
        do {
            let results = try (manageObjectContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>))
            if results.count > 0 {
                let match = results[0] as! Person
                addressTextField.text = match.address
                phoneTextField.text = match.phone
                nameTextField.text = match.name
                
            }else{
                showToast(message: "\(nameTextField.text ?? "") no existe")
                addressTextField.text = "n/a"
                phoneTextField.text = "n/a"
            }
            
        } catch {
            showToast(message: "Error al buscar")
        }
    }
}

